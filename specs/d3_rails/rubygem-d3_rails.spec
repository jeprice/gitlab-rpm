# Generated from d3_rails-3.4.6.gem by gem2rpm -*- rpm-spec -*-
%global gem_name d3_rails

Name: rubygem-%{gem_name}
Version: 3.4.6
Release: 1%{?dist}
Summary: D3 automated install for Rails 3.1+
Group: Development/Languages
License: MIT
URL: https://github.com/logical42/d3_rails
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(railties) >= 3.1.0
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(rails) >= 3.1
# BuildRequires: rubygem(pry)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Gem installation of javascript framework for data visualization, D3.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 3.4.6-1
- Initial package
