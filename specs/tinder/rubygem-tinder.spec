# Generated from tinder-1.9.4.gem by gem2rpm -*- rpm-spec -*-
%global gem_name tinder

Name: rubygem-%{gem_name}
Version: 1.9.4
Release: 1%{?dist}
Summary: Ruby wrapper for the Campfire API
Group: Development/Languages
License:
URL: http://github.com/collectiveidea/tinder
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) >= 1.3.6
Requires: rubygem(eventmachine) => 1.0
Requires: rubygem(eventmachine) < 2
Requires: rubygem(faraday) => 0.8.9
Requires: rubygem(faraday) < 0.9
Requires: rubygem(faraday_middleware) => 0.9
Requires: rubygem(faraday_middleware) < 1
Requires: rubygem(hashie) >= 1.0
Requires: rubygem(hashie) < 3
Requires: rubygem(json) => 1.8.0
Requires: rubygem(json) < 1.9
Requires: rubygem(mime-types) => 1.19
Requires: rubygem(mime-types) < 2
Requires: rubygem(multi_json) => 1.7
Requires: rubygem(multi_json) < 2
Requires: rubygem(twitter-stream) => 0.1
Requires: rubygem(twitter-stream) < 1
BuildRequires: ruby(release)
BuildRequires: rubygems-devel >= 1.3.6
BuildRequires: ruby
# BuildRequires: rubygem(fakeweb)
# BuildRequires: rubygem(rspec)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
A Ruby API for interfacing with Campfire, the 37Signals chat application.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.markdown

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 1.9.4-1
- Initial package
