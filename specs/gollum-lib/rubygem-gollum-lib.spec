# Generated from gollum-lib-3.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name gollum-lib

Name: rubygem-%{gem_name}
Version: 3.0.0
Release: 1%{?dist}
Summary: A simple, Git-powered wiki
Group: Development/Languages
License: MIT
URL: http://github.com/gollum/gollum-lib
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(gitlab-grit) => 2.6.5
Requires: rubygem(gitlab-grit) < 2.7
Requires: rubygem(rouge) => 1.3.3
Requires: rubygem(rouge) < 1.4
Requires: rubygem(nokogiri) => 1.6.1
Requires: rubygem(nokogiri) < 1.7
Requires: rubygem(stringex) => 2.5.1
Requires: rubygem(stringex) < 2.6
Requires: rubygem(sanitize) => 2.1.0
Requires: rubygem(sanitize) < 2.2
Requires: rubygem(github-markup) => 1.1.0
Requires: rubygem(github-markup) < 1.2
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby >= 1.9
# BuildRequires: rubygem(org-ruby) => 0.9.3
# BuildRequires: rubygem(org-ruby) < 0.10
# BuildRequires: rubygem(github-markdown) => 0.6.5
# BuildRequires: rubygem(github-markdown) < 0.7
# BuildRequires: rubygem(RedCloth) => 4.2.9
# BuildRequires: rubygem(RedCloth) < 4.3
# BuildRequires: rubygem(mocha) => 1.0.0
# BuildRequires: rubygem(mocha) < 1.1
# BuildRequires: rubygem(shoulda) => 3.5.0
# BuildRequires: rubygem(shoulda) < 3.6
# BuildRequires: rubygem(wikicloth) => 0.8.1
# BuildRequires: rubygem(wikicloth) < 0.9
# BuildRequires: rubygem(pry) => 0.9.12
# BuildRequires: rubygem(pry) < 0.10
# BuildRequires: rubygem(rb-readline) => 0.5.1
# BuildRequires: rubygem(rb-readline) < 0.6
# BuildRequires: rubygem(minitest-reporters) => 0.14.16
# BuildRequires: rubygem(minitest-reporters) < 0.15
# BuildRequires: rubygem(nokogiri-diff) => 0.2.0
# BuildRequires: rubygem(nokogiri-diff) < 0.3
# BuildRequires: rubygem(guard) => 2.6.0
# BuildRequires: rubygem(guard) < 2.7
# BuildRequires: rubygem(guard-minitest) => 2.2.0
# BuildRequires: rubygem(guard-minitest) < 2.3
# BuildRequires: rubygem(rb-inotify) => 0.9.3
# BuildRequires: rubygem(rb-inotify) < 0.10
# BuildRequires: rubygem(rb-fsevent) => 0.9.4
# BuildRequires: rubygem(rb-fsevent) < 0.10
# BuildRequires: rubygem(rb-fchange) => 0.0.6
# BuildRequires: rubygem(rb-fchange) < 0.1
# BuildRequires: rubygem(twitter_cldr) => 2.4.2
# BuildRequires: rubygem(twitter_cldr) < 2.5
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
A simple, Git-powered wiki with a sweet API and local frontend.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md
%doc %{gem_instdir}/LICENSE

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 3.0.0-1
- Initial package
